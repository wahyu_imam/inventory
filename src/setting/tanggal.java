/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package setting;

import java.text.SimpleDateFormat;
import java.util.Date;
import datechooser.beans.DateChooserCombo;
import java.util.Calendar;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author imam
 */
@Data
public class tanggal {
    
    @Autowired
    private SimpleDateFormat s;
    
    @Autowired
    private Date date;
    
    public tanggal(){
        date = new Date();
    }
    
    public String tglSekarang(String format){
        s = new SimpleDateFormat(format);
        String data = s.format(date);
        return data;
    }
    
    public String getDateChooser(DateChooserCombo dateChooser){
        s = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = dateChooser.getSelectedDate();
        return s.format(cal.getTime());
    }
    
    public String getDateChooserCustomFormat(DateChooserCombo dateChooser, String format){
        s = new SimpleDateFormat(format);
        Calendar cal = dateChooser.getSelectedDate();
        return s.format(cal.getTime());
    }
}
