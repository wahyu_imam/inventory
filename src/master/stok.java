/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package master;

import database.query;
import database.crud;
/**
 *
 * @author imam
 */
public class stok extends crud{
    query db;
    
    public stok(){
        db = new query();
    }
    
    public String[][] get_all(){
        String[] field = {"nama_barang","' ('","nama_kategori","')'"};
        String sql = db.select("kode_stok, "+db.concat(field)+", stok, nama_gudang");
        sql += db.from("stok");
        sql += db.join("barang", "barang.kode_barang=stok.kode_barang");
        sql += db.join("kategori", "kategori.kode_kategori=barang.kode_kategori");
        sql += db.join("gudang", "gudang.kode_gudang=stok.kode_gudang");
        sql += db.orderBy("nama_gudang", "ASC");
        String[][] data = getAll(sql, 4, "Error get_all stok");
        return data;
    }
    
    public String[] get_id(String kode_stok){
        String sql = db.select("*");
        sql += db.from("stok");
        sql += db.where("kode_stok", kode_stok);
        String[] data = getById(sql, 4, "Error get_id stok");
        return data;
    }
    
    public String[][] getByGudang(String kode_gudang){
        String[] field = {"nama_barang","' ('","nama_kategori","')'"};
        String sql = db.select("kode_stok, "+db.concat(field)+", stok");
        sql += db.from("stok");
        sql += db.join("barang", "barang.kode_barang=stok.kode_barang");
        sql += db.join("kategori", "kategori.kode_kategori=barang.kode_kategori");
        sql += db.join("gudang", "gudang.kode_gudang=stok.kode_gudang");
        sql += db.where("gudang.kode_gudang", kode_gudang);
        sql += db.orderBy("nama_gudang", "ASC");
        String[][] data = getAll(sql, 3, "Error getByGudang stok");
        return data;
    }
    
    public String[] getStok(String kode_barang, String kode_gudang){
        String sql = db.select("*");
        sql += db.from("stok");
        sql += db.where("kode_barang", kode_barang);
        sql += db.whereAnd("kode_gudang", kode_gudang);
        String[] data = getById(sql, 4, "Error getStok stok");
        return data;
    }
    
    public void save(String[][] data){
        String sql = db.insert("stok", data);
        simpanDtl(sql);
    }
    
    public void edit(String[][] data, String kode_stok){
        String sql = db.update("stok", data);
        sql += db.where("kode_stok", kode_stok);
        ubahDtl(sql);
    }
    
    public void delete(String kode_stok){
        String[] data = {"kode_stok", kode_stok};
        String sql = db.delete("stok", data);
        hapus(sql);
    }
    
    public String[][] getJmlStokGroupByKategori(String kodeCabang){
        String sql = db.query("SELECT `nama_kategori`, SUM(stok) AS jml_stok FROM kategori "
                + "JOIN barang ON barang.`kode_kategori`=`kategori`.`kode_kategori` "
                + "JOIN stok ON `stok`.`kode_barang`=`barang`.`kode_barang` "
                + "JOIN gudang ON `gudang`.`kode_gudang`=`stok`.`kode_gudang` "
                + "WHERE gudang.`kode_gudang`='"+kodeCabang+"' "
                + "GROUP BY `nama_kategori`");
        String[][] data = getAll(sql, 2, "Error getJmlStokGroupByKategori stok");
        return data;
    }
    
    public String[][] getJmlStok(){
        String sql = db.query("SELECT `nama_kategori`, SUM(stok) AS jml_stok FROM kategori "
                + "JOIN barang ON barang.`kode_kategori`=`kategori`.`kode_kategori` "
                + "JOIN stok ON `stok`.`kode_barang`=`barang`.`kode_barang` "
                + "JOIN gudang ON `gudang`.`kode_gudang`=`stok`.`kode_gudang` "
                + "GROUP BY `nama_kategori`");
        String[][] data = getAll(sql, 2, "Error getJmlStokGroupByKategori stok");
        return data;
    }
    
    public String[][] getAllStokRelasiAllCabang(){
        String sql = db.query("SELECT stok.`kode_stok` AS `kode_stok`, "
                + "CONCAT(`nama_barang`,' (',`nama_kategori`,')') AS `nama_barang`, "
                + "`nama_gudang`, stok FROM barang "
                + "JOIN stok ON `stok`.`kode_barang`=`barang`.`kode_barang` "
                + "JOIN gudang ON `gudang`.`kode_gudang`=`stok`.`kode_gudang` "
                + "JOIN `kategori` ON `kategori`.`kode_kategori`=`barang`.`kode_kategori` "
                + "ORDER BY `nama_gudang` ASC");
        String[][] data = getAll(sql, 4, "Error getAllStokRelasiAllCabang stok");
        return data;
    }
    
    public String[][] getLimitStok(String kodeGudang){
        String sql = db.query("SELECT `nama_barang`, `nama_kategori`, `stok`" 
                + "FROM `stok` "
                + "JOIN `barang` ON `barang`.`kode_barang`=`stok`.`kode_barang` "
                + "JOIN `kategori` ON `kategori`.`kode_kategori`=`barang`.`kode_kategori` "
                + "WHERE `stok` < 500 "
                + "AND `kode_gudang`='"+kodeGudang+"'");
        String[][] data = getAll(sql, 3, "Error GetLimitStok stok");
        return data;
    }
}
