/*
SQLyog Ultimate v12.5.1 (32 bit)
MySQL - 10.1.19-MariaDB : Database - inventory
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`inventory` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `inventory`;

/*Table structure for table `barang` */

DROP TABLE IF EXISTS `barang`;

CREATE TABLE `barang` (
  `kode_barang` varchar(20) NOT NULL,
  `nama_barang` varchar(50) DEFAULT NULL,
  `kode_kategori` varchar(20) DEFAULT NULL,
  `satuan` varchar(10) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  PRIMARY KEY (`kode_barang`),
  KEY `kode_kategori` (`kode_kategori`),
  CONSTRAINT `barang_ibfk_1` FOREIGN KEY (`kode_kategori`) REFERENCES `kategori` (`kode_kategori`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `barang` */

insert  into `barang`(`kode_barang`,`nama_barang`,`kode_kategori`,`satuan`,`harga`) values 
('A8L9M2M194L4M94','BB 01','G01KK866L04460M','KG',11000),
('B0MK8114816LL6K','BA 02','IK49LL8006M0KMK','KG',14500),
('B849201MMM40KKK','C4 A','I8649210LK662KL','KG',15000),
('C28688048611K0K','BA 01','IK49LL8006M0KMK','KG',14000),
('FKL8699890LMKL0','C4 C','G01KK866L04460M','KG',10000),
('G9L1962LMM114ML','BS 02','I8649210LK662KL','KG',17000),
('H690K10989062L2','BS 01','I8649210LK662KL','KG',16000),
('H6M28L626M96969','C4 B','IK49LL8006M0KMK','KG',13000),
('ILK88L6M4280M0M','BB 02','G01KK866L04460M','KG',12000);

/*Table structure for table `dtl_return` */

DROP TABLE IF EXISTS `dtl_return`;

CREATE TABLE `dtl_return` (
  `kode_detail` varchar(20) NOT NULL,
  `kode_return` varchar(20) DEFAULT NULL,
  `kode_barang` varchar(20) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  PRIMARY KEY (`kode_detail`),
  KEY `kode_return` (`kode_return`),
  KEY `kode_barang` (`kode_barang`),
  CONSTRAINT `dtl_return_ibfk_1` FOREIGN KEY (`kode_return`) REFERENCES `return` (`kode_return`),
  CONSTRAINT `dtl_return_ibfk_2` FOREIGN KEY (`kode_barang`) REFERENCES `barang` (`kode_barang`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dtl_return` */

insert  into `dtl_return`(`kode_detail`,`kode_return`,`kode_barang`,`qty`) values 
('BKMK812L10M4699','C4699K4L220LM19','H6M28L626M96969',10),
('F694K098K14K10K','C9129LK986K96K1','B849201MMM40KKK',2),
('GM944M689L10L12','FMKM2L2K88K692M','B849201MMM40KKK',50000),
('J9281024842LKK4','C4699K4L220LM19','B849201MMM40KKK',10);

/*Table structure for table `dtl_tr_keluar` */

DROP TABLE IF EXISTS `dtl_tr_keluar`;

CREATE TABLE `dtl_tr_keluar` (
  `kode_detail` varchar(20) NOT NULL,
  `kode_transaksi` varchar(20) DEFAULT NULL,
  `kode_barang` varchar(20) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `sub_total` int(11) DEFAULT NULL,
  PRIMARY KEY (`kode_detail`),
  KEY `kode_transaksi` (`kode_transaksi`),
  KEY `kode_barang` (`kode_barang`),
  CONSTRAINT `dtl_tr_keluar_ibfk_1` FOREIGN KEY (`kode_transaksi`) REFERENCES `tr_keluar` (`kode_transaksi`),
  CONSTRAINT `dtl_tr_keluar_ibfk_2` FOREIGN KEY (`kode_barang`) REFERENCES `barang` (`kode_barang`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dtl_tr_keluar` */

insert  into `dtl_tr_keluar`(`kode_detail`,`kode_transaksi`,`kode_barang`,`qty`,`sub_total`) values 
('AMK008L6M814LL2','J0KLL21261M04M8','FKL8699890LMKL0',4,40000),
('C8442L2KKKL1816','D6K60M42LM046K1','B849201MMM40KKK',10,NULL),
('C8680K1LM1426M6','I26MK60M116L9KM','H6M28L626M96969',5,NULL),
('CM2M8K6K80MK2L2','I4M8919M0916L66','B849201MMM40KKK',6000,90000000),
('D9M08L1K9418168','I26MK60M116L9KM','B849201MMM40KKK',10,NULL),
('DL68K42M9422962','HM6K9L8990L499L','B849201MMM40KKK',20,NULL),
('E2M9M4149920249','B2492L46100M0K4','B849201MMM40KKK',5,75000),
('E8L18426806018K','H2KL1KL22LM21KL','B849201MMM40KKK',3,45000),
('F1064216K9MKMM0','B2804806M4L9014','B849201MMM40KKK',1,15000),
('F4K282KK0028LLM','C84K48461L4ML49','B849201MMM40KKK',10,NULL),
('G098K8984L6142K','H61KKM9LM08K689','H6M28L626M96969',2,26000),
('G88949M9996K0K6','BL0M6911006L6KL','H6M28L626M96969',1,13000),
('GL94LKK8200408M','DM4M10L94L8M66L','H6M28L626M96969',2,26000),
('H11K4649M4KL1K9','J4MLM11918M0046','H6M28L626M96969',60,780000),
('H89L9L064K41M90','JL0MM12089LLM14','FKL8699890LMKL0',3,30000),
('HLL4K6K28LL0226','A606M642M9M4284','FKL8699890LMKL0',5,50000),
('HM9LL8K860LL9M1','G608L11910L896L','B849201MMM40KKK',1000,NULL),
('I111824M8942M6K','H8499M1088L92L8','B849201MMM40KKK',4,60000),
('I60816M8K08K610','H2KL1KL22LM21KL','FKL8699890LMKL0',3,30000),
('IM99L0KM981840M','AK86L8K9080KKM4','B849201MMM40KKK',1,15000),
('JK0964066K022M6','HM6K9L8990L499L','H6M28L626M96969',20,NULL);

/*Table structure for table `dtl_tr_masuk` */

DROP TABLE IF EXISTS `dtl_tr_masuk`;

CREATE TABLE `dtl_tr_masuk` (
  `kode_detail` varchar(20) NOT NULL,
  `kode_transaksi` varchar(20) DEFAULT NULL,
  `kode_barang` varchar(20) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `sub_total` int(11) DEFAULT NULL,
  PRIMARY KEY (`kode_detail`),
  KEY `kode_transaksi` (`kode_transaksi`),
  KEY `kode_barang` (`kode_barang`),
  CONSTRAINT `dtl_tr_masuk_ibfk_1` FOREIGN KEY (`kode_transaksi`) REFERENCES `tr_masuk` (`kode_transaksi`),
  CONSTRAINT `dtl_tr_masuk_ibfk_2` FOREIGN KEY (`kode_barang`) REFERENCES `barang` (`kode_barang`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dtl_tr_masuk` */

insert  into `dtl_tr_masuk`(`kode_detail`,`kode_transaksi`,`kode_barang`,`qty`,`sub_total`) values 
('B8908000LMM1K80','AK46M4L6984698K','ILK88L6M4280M0M',100,150000),
('B89M8L9LK0448LL','I1M0K02L9MK861K','H6M28L626M96969',20,NULL),
('BK982M04L4M4L62','AK46M4L6984698K','B0MK8114816LL6K',100,100000),
('C4441MLMLL24LKK','AK46M4L6984698K','C28688048611K0K',100,100000),
('C9KL6LMMM1K8K89','D49K669124K4281','H6M28L626M96969',100,150000),
('CKK9M9MM2L10L46','D49K669124K4281','FKL8699890LMKL0',100,100000),
('D186MKMK81K829M','AK46M4L6984698K','G9L1962LMM114ML',100,200000),
('D64682229M961L1','H01260KK94LK894','B849201MMM40KKK',50000,NULL),
('E8KL04920L60429','JK1912662994481','H6M28L626M96969',100,1000000),
('FM62M00862MML09','JK1912662994481','B849201MMM40KKK',100,1300000),
('FMLKK4LKM29K168','J61ML1989418K92','B849201MMM40KKK',10,NULL),
('G1MKM9800K2KM46','D9LM4M104618681','B849201MMM40KKK',10,NULL),
('GK9K28221M61680','FMK29K01119M616','B849201MMM40KKK',50,700000),
('H0M869L146L2141','J61ML1989418K92','H6M28L626M96969',5,NULL),
('H282K4K81K81026','AK46M4L6984698K','H690K10989062L2',100,200000),
('H6106216L2M2K9M','JK1912662994481','FKL8699890LMKL0',100,700000),
('H681K98MLLL8198','D49K669124K4281','B849201MMM40KKK',100,200000),
('H89K2924M1K8429','I1M0K02L9MK861K','B849201MMM40KKK',20,NULL),
('I60646664M41990','AK46M4L6984698K','A8L9M2M194L4M94',100,150000),
('IL662ML8069L281','I1K8K0200L10K8L','B849201MMM40KKK',10,NULL),
('IM424482692K06M','I1K8K0200L10K8L','H6M28L626M96969',10,NULL),
('J6641L46KLLMM11','C2KLKK92K882L8K','B849201MMM40KKK',1000,NULL);

/*Table structure for table `gudang` */

DROP TABLE IF EXISTS `gudang`;

CREATE TABLE `gudang` (
  `kode_gudang` varchar(20) NOT NULL,
  `nama_gudang` varchar(50) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `status_gudang` enum('Pusat','Cabang') DEFAULT NULL,
  PRIMARY KEY (`kode_gudang`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `gudang` */

insert  into `gudang`(`kode_gudang`,`nama_gudang`,`alamat`,`status_gudang`) values 
('A94424LKL2L8120','Gudang Cabang 1','Jl. Magelang','Cabang'),
('E9MK88L48L0LK19','Gudang Pusat','Jl. Pamotan rembang','Pusat'),
('J8081KM21106981','Gudang Cabang 2','Coba','Cabang');

/*Table structure for table `kategori` */

DROP TABLE IF EXISTS `kategori`;

CREATE TABLE `kategori` (
  `kode_kategori` varchar(20) NOT NULL,
  `nama_kategori` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`kode_kategori`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `kategori` */

insert  into `kategori`(`kode_kategori`,`nama_kategori`) values 
('1','test'),
('G01KK866L04460M','Beras Biasa'),
('I8649210LK662KL','Beras Super'),
('IK49LL8006M0KMK','Beras Baik');

/*Table structure for table `petugas` */

DROP TABLE IF EXISTS `petugas`;

CREATE TABLE `petugas` (
  `kode_petugas` varchar(20) NOT NULL,
  `nama_petugas` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `pass` varchar(50) DEFAULT NULL,
  `status` enum('Aktif','Tidak Aktif') DEFAULT NULL,
  `kode_gudang` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`kode_petugas`),
  KEY `kode_cabang` (`kode_gudang`),
  CONSTRAINT `petugas_ibfk_1` FOREIGN KEY (`kode_gudang`) REFERENCES `gudang` (`kode_gudang`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `petugas` */

insert  into `petugas`(`kode_petugas`,`nama_petugas`,`username`,`pass`,`status`,`kode_gudang`) values 
('A1K00M408488K4L','udin','udin','6bec9c852847242e384a4d5ac0962ba0','Aktif','A94424LKL2L8120'),
('B826LL9690M489L','imam','admin','21232f297a57a5a743894a0e4a801fc3','Aktif','E9MK88L48L0LK19'),
('CL82L8KL92848LK','wahyu','wahyu','32c9e71e866ecdbc93e497482aa6779f','Aktif','J8081KM21106981');

/*Table structure for table `return` */

DROP TABLE IF EXISTS `return`;

CREATE TABLE `return` (
  `kode_return` varchar(20) NOT NULL,
  `tgl_return` datetime DEFAULT NULL,
  `kode_petugas` varchar(20) DEFAULT NULL,
  `konfirmasi_pusat` enum('Belum','Sudah') DEFAULT NULL,
  PRIMARY KEY (`kode_return`),
  KEY `kode_petugas` (`kode_petugas`),
  CONSTRAINT `return_ibfk_1` FOREIGN KEY (`kode_petugas`) REFERENCES `petugas` (`kode_petugas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `return` */

insert  into `return`(`kode_return`,`tgl_return`,`kode_petugas`,`konfirmasi_pusat`) values 
('C4699K4L220LM19','2021-04-17 04:35:56','A1K00M408488K4L','Sudah'),
('C9129LK986K96K1','2021-04-24 11:42:04','A1K00M408488K4L','Belum'),
('FMKM2L2K88K692M','2021-04-26 10:11:28','A1K00M408488K4L','Sudah');

/*Table structure for table `stok` */

DROP TABLE IF EXISTS `stok`;

CREATE TABLE `stok` (
  `kode_stok` varchar(20) NOT NULL,
  `kode_barang` varchar(20) DEFAULT NULL,
  `stok` int(11) DEFAULT NULL,
  `kode_gudang` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`kode_stok`),
  KEY `kode_barang` (`kode_barang`),
  KEY `kode_cabang` (`kode_gudang`),
  CONSTRAINT `stok_ibfk_1` FOREIGN KEY (`kode_barang`) REFERENCES `barang` (`kode_barang`),
  CONSTRAINT `stok_ibfk_2` FOREIGN KEY (`kode_gudang`) REFERENCES `gudang` (`kode_gudang`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `stok` */

insert  into `stok`(`kode_stok`,`kode_barang`,`stok`,`kode_gudang`) values 
('B66220L661LMM11','B849201MMM40KKK',10,'A94424LKL2L8120'),
('BK14K2M1669K098','FKL8699890LMKL0',185,'E9MK88L48L0LK19'),
('BK4M1084L1M1169','H6M28L626M96969',10,'A94424LKL2L8120'),
('BMK2289L8K6K818','B849201MMM40KKK',43202,'E9MK88L48L0LK19'),
('D1K2L46421M6249','G9L1962LMM114ML',100,'E9MK88L48L0LK19'),
('D609KMK2191881L','B0MK8114816LL6K',100,'E9MK88L48L0LK19'),
('EL64K8L8096K00K','A8L9M2M194L4M94',100,'E9MK88L48L0LK19'),
('F1KKL68MM12446M','H690K10989062L2',100,'E9MK88L48L0LK19'),
('G12L868MM421M61','ILK88L6M4280M0M',100,'E9MK88L48L0LK19'),
('HM1MK48M4828919','C28688048611K0K',100,'E9MK88L48L0LK19'),
('J092L00101460M6','H6M28L626M96969',125,'E9MK88L48L0LK19');

/*Table structure for table `supplier` */

DROP TABLE IF EXISTS `supplier`;

CREATE TABLE `supplier` (
  `kode_supplier` varchar(20) NOT NULL,
  `nama_supplier` varchar(50) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `no_tlp` varchar(13) DEFAULT NULL,
  PRIMARY KEY (`kode_supplier`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `supplier` */

insert  into `supplier`(`kode_supplier`,`nama_supplier`,`alamat`,`no_tlp`) values 
('D60660K604M24L4','John','Jl. John','087848657335'),
('DK16L81L9M12M11','Titin','Jl. Monjali','089765473643');

/*Table structure for table `tr_keluar` */

DROP TABLE IF EXISTS `tr_keluar`;

CREATE TABLE `tr_keluar` (
  `kode_transaksi` varchar(20) NOT NULL,
  `tgl_transaksi` datetime DEFAULT NULL,
  `kode_gudang` varchar(20) DEFAULT NULL,
  `kode_petugas` varchar(20) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `bayar` int(11) DEFAULT NULL,
  `konfirmasi_cabang` enum('Belum','Sudah') DEFAULT NULL,
  PRIMARY KEY (`kode_transaksi`),
  KEY `kode_cabang` (`kode_gudang`),
  KEY `kode_petugas` (`kode_petugas`),
  CONSTRAINT `tr_keluar_ibfk_2` FOREIGN KEY (`kode_gudang`) REFERENCES `gudang` (`kode_gudang`),
  CONSTRAINT `tr_keluar_ibfk_3` FOREIGN KEY (`kode_petugas`) REFERENCES `petugas` (`kode_petugas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tr_keluar` */

insert  into `tr_keluar`(`kode_transaksi`,`tgl_transaksi`,`kode_gudang`,`kode_petugas`,`total`,`bayar`,`konfirmasi_cabang`) values 
('A606M642M9M4284','2021-05-18 10:53:58',NULL,'B826LL9690M489L',50000,50000,NULL),
('AK86L8K9080KKM4','2021-04-19 07:53:43',NULL,'B826LL9690M489L',15000,15000,NULL),
('B2492L46100M0K4','2021-04-17 05:05:16',NULL,'A1K00M408488K4L',75000,100000,NULL),
('B2804806M4L9014','2021-04-19 07:25:55',NULL,'A1K00M408488K4L',15000,15000,NULL),
('BL0M6911006L6KL','2021-04-19 07:24:34',NULL,'A1K00M408488K4L',13000,15000,NULL),
('C84K48461L4ML49','2021-04-26 10:05:59','A94424LKL2L8120','B826LL9690M489L',NULL,NULL,'Sudah'),
('D6K60M42LM046K1','2021-05-18 10:47:15','A94424LKL2L8120','B826LL9690M489L',NULL,NULL,'Belum'),
('DM4M10L94L8M66L','2021-04-19 07:22:10',NULL,'A1K00M408488K4L',26000,30000,NULL),
('G608L11910L896L','2021-04-26 10:08:36','A94424LKL2L8120','B826LL9690M489L',NULL,NULL,'Sudah'),
('H2KL1KL22LM21KL','2021-04-19 07:49:37',NULL,'B826LL9690M489L',75000,100000,NULL),
('H61KKM9LM08K689','2021-04-19 07:27:56',NULL,'A1K00M408488K4L',26000,30000,NULL),
('H8499M1088L92L8','2021-04-17 06:28:21',NULL,'B826LL9690M489L',60000,100000,NULL),
('HM6K9L8990L499L','2021-04-17 03:55:30','A94424LKL2L8120','B826LL9690M489L',NULL,NULL,'Sudah'),
('I26MK60M116L9KM','2021-04-24 11:39:50','A94424LKL2L8120','B826LL9690M489L',NULL,NULL,'Sudah'),
('I4M8919M0916L66','2021-04-26 10:18:28',NULL,'B826LL9690M489L',90000000,100000000,NULL),
('J0KLL21261M04M8','2021-04-19 07:51:17',NULL,'B826LL9690M489L',40000,50000,NULL),
('J4MLM11918M0046','2021-05-18 11:04:40',NULL,'B826LL9690M489L',780000,780000,NULL),
('JL0MM12089LLM14','2021-04-17 03:57:03',NULL,'B826LL9690M489L',30000,30000,NULL);

/*Table structure for table `tr_masuk` */

DROP TABLE IF EXISTS `tr_masuk`;

CREATE TABLE `tr_masuk` (
  `kode_transaksi` varchar(20) NOT NULL,
  `tgl_transaksi` datetime DEFAULT NULL,
  `kode_supplier` varchar(20) DEFAULT NULL,
  `kode_petugas` varchar(20) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  PRIMARY KEY (`kode_transaksi`),
  KEY `kode_supplier` (`kode_supplier`),
  KEY `kode_petugas` (`kode_petugas`),
  CONSTRAINT `tr_masuk_ibfk_1` FOREIGN KEY (`kode_supplier`) REFERENCES `supplier` (`kode_supplier`),
  CONSTRAINT `tr_masuk_ibfk_2` FOREIGN KEY (`kode_petugas`) REFERENCES `petugas` (`kode_petugas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tr_masuk` */

insert  into `tr_masuk`(`kode_transaksi`,`tgl_transaksi`,`kode_supplier`,`kode_petugas`,`total`) values 
('AK46M4L6984698K','2021-06-20 10:10:44','D60660K604M24L4','B826LL9690M489L',900000),
('C2KLKK92K882L8K','2021-04-26 10:10:47',NULL,'A1K00M408488K4L',0),
('D49K669124K4281','2021-06-20 10:04:05','DK16L81L9M12M11','B826LL9690M489L',450000),
('D9LM4M104618681','2021-04-26 10:06:20',NULL,'A1K00M408488K4L',0),
('FMK29K01119M616','2021-04-17 01:14:03','DK16L81L9M12M11','B826LL9690M489L',700000),
('H01260KK94LK894','2021-04-26 10:12:43',NULL,'B826LL9690M489L',0),
('I1K8K0200L10K8L','2021-04-17 04:59:22',NULL,'B826LL9690M489L',0),
('I1M0K02L9MK861K','2021-04-17 04:01:22',NULL,'A1K00M408488K4L',0),
('J61ML1989418K92','2021-04-24 11:41:24',NULL,'A1K00M408488K4L',0),
('JK1912662994481','2021-04-17 01:10:08','DK16L81L9M12M11','B826LL9690M489L',3000000);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
